<?php

namespace MainBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

use MainBundle\Entity\Response;


class ResponseListener
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     *
     * @param LifecycleEventArgs $args
     */
    public function prePersist(Response $entity, LifecycleEventArgs $args)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $offer = $em->getRepository('MainBundle:Offer')->find($entity->getReference());
        if ($offer !== null) {
            $entity->setOffer($offer);
        }

        if ($entity->getExtraInfo() != 0) {
            $extraInfo = $em->getRepository('MainBundle:ExtraInfo')->findOneById($entity->getExtraInfo());
            $entity->setExtraInfo($extraInfo);
        }

        $user = $this->container->get('security.context')->getToken()->getUser();
        $entity->setUser($user);
    }


}