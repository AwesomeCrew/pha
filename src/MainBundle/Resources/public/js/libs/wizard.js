/*
 * Fuel UX Wizard
 * https://github.com/ExactTarget/fuelux
 *
 * Copyright (c) 2012 ExactTarget
 * Licensed under the MIT license.
 */

// WIZARD CONSTRUCTOR AND PROTOTYPE

var Wizard = function (element, options) {
	var kids;

	this.$element = $(element);
	this.options = $.extend({}, $.fn.wizard.defaults, options);
	this.currentStep = this.options.selectedItem.step;
	this.logging = this.options.logging;
	this.numSteps = this.$element.find('.steps li').length;
	this.$prevBtn = this.$element.find('button.btn-prev');
	this.$nextBtn = this.$element.find('button.btn-next');

	kids = this.$nextBtn.children().detach();
	this.nextText = $.trim(this.$nextBtn.text());
	this.$nextBtn.append(kids);

	// handle events
	this.$prevBtn.on('click', $.proxy(this.previous, this));
	this.$nextBtn.on('click', $.proxy(this.next, this));
	this.$element.on('click', 'li.complete', $.proxy(this.stepclicked, this));

	if(this.currentStep > 1) {
        this.selectedItem(this.options.selectedItem);
	}
};

Wizard.prototype = {

	constructor: Wizard,

	setState: function () {
		var canMovePrev = (this.currentStep > 1);
		var firstStep = (this.currentStep === 1);
		var lastStep = (this.currentStep === this.numSteps);

		// disable buttons based on current step
		this.$prevBtn.attr('disabled', (firstStep === true || canMovePrev === false));

		// change button text of last step, if specified
		var data = this.$nextBtn.data();
		if (data && data.last) {
			this.lastText = data.last;
			if (typeof this.lastText !== 'undefined') {
				// replace text
				var text = (lastStep !== true) ? this.nextText : this.lastText;
				var kids = this.$nextBtn.children().detach();
				this.$nextBtn.text(text).append(kids);
			}
		}

		// reset classes for all steps
		var $steps = this.$element.find('.steps li');
		$steps.removeClass('active').removeClass('complete');
		$steps.find('span.badge').removeClass('badge-primary').removeClass('badge-success');

		// set class for all previous steps
		var prevSelector = '.steps li:lt(' + (this.currentStep - 1) + ')';
		var $prevSteps = this.$element.find(prevSelector);
		$prevSteps.addClass('complete');
		$prevSteps.find('span.badge').addClass('badge-success');

		// set class for current step
		var currentSelector = '.steps li:eq(' + (this.currentStep - 1) + ')';
		var $currentStep = this.$element.find(currentSelector);
		$currentStep.addClass('active');
		$currentStep.find('span.badge').addClass('badge-primary');

		// set display of target element
		var target = $currentStep.data().target;
		this.$element.find('.step-pane').removeClass('active');
		$(target).addClass('active');

		// reset the wizard position to the left
		$('.wizard .steps').attr('style','margin-left: 0');

		// check if the steps are wider than the container div
		var totalWidth = 0;
		$('.wizard .steps > li').each(function () {
			totalWidth += $(this).outerWidth();
		});
		var containerWidth = 0;
		if ($('.wizard .actions').length) {
			containerWidth = $('.wizard').width() - $('.wizard .actions').outerWidth();
		} else {
			containerWidth = $('.wizard').width();
		}
		if (totalWidth > containerWidth) {

			// set the position so that the last step is on the right
			var newMargin = totalWidth - containerWidth;
			$('.wizard .steps').attr('style','margin-left: -' + newMargin + 'px');

			// set the position so that the active step is in a good
			// position if it has been moved out of view
			if ($('.wizard li.active').position().left < 200) {
				newMargin += $('.wizard li.active').position().left - 200;
				if (newMargin < 1) {
					$('.wizard .steps').attr('style','margin-left: 0');
				} else {
					$('.wizard .steps').attr('style','margin-left: -' + newMargin + 'px');
				}
			}
		}

		this.$element.trigger('changed');
	},

	stepclicked: function (e) {
		var li = $(e.currentTarget);

		var index = this.$element.find('.steps li').index(li);
		this.log(index);
		var evt = $.Event('stepclick');
		this.$element.trigger(evt, {step: index + 1});
		if (evt.isDefaultPrevented()) return;

		this.currentStep = (index + 1);
		this.setState();
	},

	previous: function () {
		this.log("previous");
		var canMovePrev = (this.currentStep > 1);
		if (canMovePrev) {
			var e = $.Event('change');
			this.$element.trigger(e, {step: this.currentStep, direction: 'previous'});
			if (e.isDefaultPrevented()) return;

			this.currentStep -= 1;
			this.setState();
		}
	},

	next: function () {
		var canMoveNext = (this.currentStep + 1 <= this.numSteps);
		var lastStep = (this.currentStep === this.numSteps);
		this.log("next");

		if (canMoveNext) {
			var stepContainerId = this.options.stepContainerId[this.currentStep];
			var result = this.validateStep(stepContainerId);
			if (!result) return;

			var e = $.Event('change');
			this.$element.trigger(e, {step: this.currentStep, direction: 'next'});

			if (e.isDefaultPrevented()) return;

			this.currentStep += 1;
			this.setState();
		}
		else if (lastStep) {
			this.log('lastStep');
			var stepContainerId = this.options.stepContainerId[this.currentStep];
			var result = this.validateStep(stepContainerId);
			if (!result) return;

			this.$element.trigger('finished');
		}
	},

	validateStep: function(id) {

        var resultTotal = true;
        var $this = this;
        $(id).find("[data-validate]").each(function(i, el) {
        	$this.log('Validate '+ id)
            el = $(el);
            var v = el.data("validate");
            if (!v) {return;}

            var fn = window[v];
            if(typeof fn === 'function'){
            	var ret = {
					status: true,
					title: "Error",
					msg: ""
				};
                var vret = fn(el);
				$.extend(ret, vret);

				if (!ret.status) {
					failures = true;
					//el.parent(".control-group").toggleClass("error", true);
					$this.errorPopover(el, ret.msg);
					resultTotal = false;
					setTimeout(function(){el.popover("destroy")}, 2000);
				}
				else {
					//el.parent(".control-group").toggleClass("error", false);
					try {
						el.popover("destroy");
					}
					/*
					 * older versions of bootstrap don't have a destroy call
					 * for popovers
					 */
					catch (e) {
						el.popover("hide");
					}
				}
            } else {
            	console.log('Function used in data-validate was not defined.')
            }
        });

        return resultTotal;
    },
	selectedItem: function (selectedItem) {
		var retVal, step;
		this.log("selectedItem");
		if(selectedItem) {

			step = selectedItem.step || -1;

			if(step >= 1 && step <= this.numSteps) {
				this.currentStep = step;
				this.setState();
			}

			retVal = this;
		}
		else {
			retVal = { step: this.currentStep };
		}

		return retVal;
	},

	log: function() {
		if (!window.console || !this.logging) {return;}
		var prepend = "Wizard";
		var args = [prepend];
		args.push.apply(args, arguments);

		console.log.apply(console, args);
	},
	errorPopover: function(el, msg) {
		this.log("launching popover on", el);
		var popover = el.popover({
			content: msg,
			placement: "top",
			trigger: "manual"
		}).popover("show").next(".popover");

		popover.addClass("error-popover");
		return popover;
	},

	destroyPopover: function(pop) {
		pop = $(pop);
		pop.parent(".control-group").toggleClass("error", false);

		/*
		 * this is the element that the popover was created for
		 */
		var el = pop.prev();

		try {
			el.popover("destroy");
		}
		/*
		 * older versions of bootstrap don't have a destroy call
		 * for popovers
		 */
		catch (e) {
			el.popover("hide");
		}
	},

	hidePopovers: function(el) {
		this.log("hiding all popovers");
		var self = this;
		this.el.find(".error-popover").each(function (i, popover) {
			self.destroyPopover(popover);
		});
	},
};


// WIZARD PLUGIN DEFINITION

$.fn.wizard = function (option, value) {
	var methodReturn;

	var $set = this.each(function () {
		var $this = $(this);
		var data = $this.data('wizard');
		var options = typeof option === 'object' && option;

		if (!data) $this.data('wizard', (data = new Wizard(this, options)));
		if (typeof option === 'string') methodReturn = data[option](value);
	});

	return (methodReturn === undefined) ? $set : methodReturn;
};

$.fn.wizard.defaults = {
    selectedItem: {step:1},
    stepContainerId: {
	    1 : '#step1',
	    2 : '#step2',
	    3 : '#step3',
	    4 : '#step4',
	    5 : '#step5'
	},
};

$.fn.wizard.Constructor = Wizard;


// WIZARD DATA-API

$(function () {
	$('body').on('mousedown.wizard.data-api', '.wizard', function () {
		var $this = $(this);
		if ($this.data('wizard')) return;
		$this.wizard($this.data());
	});
});

