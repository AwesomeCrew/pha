<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

use UserBundle\Entity\User;
use FOS\UserBundle\Model\Group as BaseGroup;

/**
 * Offre
 *
 * @ORM\Table(name="providers")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ProviderRepository")
 * @ORM\HasLifecycleCallbacks()
 * @DoctrineAssert\UniqueEntity("id")
 *
 * @author Maior Valentin <maior.valentin@gmail.com>
 * @copyright 2015 si0n
 */
class Provider extends BaseGroup{

    /**
     * Override FOSUserBundle Group base class default role.
     */
    const ROLE_FRNS = 'ROLE_FRNS';
    const ROLE_EDF  = 'ROLE_EDF';

    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\Column(type="string", name="reference")
     * @var string
     */
    public $reference;

    /**
     * @ORM\Column(type="string", name="social_reason")
     * @var string
     */
    public $socialReason;

    /**
     * @ORM\Column(type="string", name="siret")
     * @var string
     */
    public $siret;

    /**
     * @ORM\Column(type="string", name="address")
     * @var string
     */
    public $address;

    /**
     * @ORM\Column(type="integer", name="zipcode")
     * @var string
     */
    public $zipcode;

    /**
     * @ORM\Column(type="string", name="country")
     * @var string
     */
    public $country;

    /**
     * @ORM\Column(type="string", name="phone")
     * @var string
     */
    public $phone;

    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\User", cascade={"persist", "remove"}, mappedBy="provider")
     * @Assert\Valid
     */
    public $users;

    /**
     * ORM\OneToMany(targetEntity="MainBundle\Entity\MessageSent", cascade={"persist", "remove"}, mappedBy="message")
     * @Assert\Valid
     */
    //private $messages;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->setRoles(array(self::ROLE_FRNS));
        $this->addRole(self::ROLE_FRNS);
        $this->setCreatedAt(new \DateTime('now'));
        $this->setUpdatedAt(new \DateTime('now'));
        $this->messages = new ArrayCollection();
    }

    /**
     * Gets the value of reference.
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Sets the value of reference.
     *
     * @param string $reference the reference
     *
     * @return self
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Gets the value of socialReason.
     *
     * @return string
     */
    public function getSocialReason()
    {
        return $this->socialReason;
    }

    /**
     * Sets the value of socialReason.
     *
     * @param string $socialReason the social reason
     *
     * @return self
     */
    public function setSocialReason($socialReason)
    {
        $this->socialReason = $socialReason;

        return $this;
    }

    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets the value of siret.
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Gets the value of address.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Gets the value of zipcode.
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Gets the value of country.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Gets the value of phone.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets the value of siret.
     *
     * @param string $siret the siret
     *
     * @return self
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Sets the value of address.
     *
     * @param string $address the address
     *
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Sets the value of zipcode.
     *
     * @param string $zipcode the zipcode
     *
     * @return self
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Sets the value of country.
     *
     * @param string $country the country
     *
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Sets the value of phone.
     *
     * @param string $phone the phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @param User[] $Users
     */
    public function setUsers($users)
    {
        $this->users = new ArrayCollection();

        foreach ($users as $user) {
            $this->addUser($user);
        }

    }

    /**
     * Add Users
     *
     * @param \UserBundle\Entity\User $User
     * @return Role
     */
    public function addUser(User $user)
    {
        $user->setProvider($this);
        $user->setUsername($user->getEmail());
        $user->setPassword('NONE');

        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove Users
     *
     * @param \UserBundle\Entity\User $User
     */
    public function removeUser(User $User)
    {
        $this->users->removeElement($User);
        $this->setUsers($this->users);
    }

    /**
     * Get Users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }


}