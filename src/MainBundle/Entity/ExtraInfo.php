<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ExtraInfo
 *
 * @ORM\Table(name="extra_infos")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ExtraInfoRepository")
 * @DoctrineAssert\UniqueEntity("id")
 *
 * @author Maior Valentin <maior.valentin@gmail.com>
 * @copyright 2015 si0n
 */
class ExtraInfo{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\Column(type="string", name="content")
     * @var string
     */
    private $content;

    /**
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\Response", cascade={"persist", "remove"}, mappedBy="extraInfo")
     * @Assert\Valid
     */
    private $responses;

    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets the value of content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Sets the value of content.
     *
     * @param string $content the content
     *
     * @return self
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Gets the value of responses.
     *
     * @return mixed
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * Sets the value of responses.
     *
     * @param mixed $responses the responses
     *
     * @return self
     */
    public function setResponses($responses)
    {
        $this->responses = $responses;

        return $this;
    }


}