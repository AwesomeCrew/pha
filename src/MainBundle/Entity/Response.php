<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

use MainBundle\Entity\Offer;
use MainBundle\Entity\ExtraInfo;
use UserBundle\Entity\User;

/**
 * Product
 *
 * @ORM\Table(name="responses")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ResponseRepository")
 * @DoctrineAssert\UniqueEntity("id")
 * @ORM\EntityListeners({"MainBundle\Listener\ResponseListener"})
 *
 * @author Maior Valentin <maior.valentin@gmail.com>
 * @copyright 2015 si0n
 */
class Response{

    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
    * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Offer", inversedBy="responses")
    * @ORM\JoinColumn(name="offer_id", onDelete="CASCADE")
    */
    private $offer;

    /**
    * @var string
    */
    private $reference;

    /**
     * @ORM\Column(type="integer", name="last_step")
     * @var integer
     */
    private $lastStep;

    /**
    * @ORM\ManyToOne(targetEntity="MainBundle\Entity\ExtraInfo", inversedBy="responses")
    * @ORM\JoinColumn(name="extra_info_id", onDelete="CASCADE", nullable=true)
    */
    private $extraInfo;

    /**
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\Proposal", cascade={"persist", "remove"}, mappedBy="response")
     * @Assert\Valid
     */
    private $proposals;

    /**
    * @ORM\ManyToOne(targetEntity="USerBundle\Entity\User", inversedBy="responses")
    * @ORM\JoinColumn(name="user_id", onDelete="CASCADE")
    */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->proposals = new ArrayCollection();
        $this->setCreatedAt(new \DateTime('now'));
        $this->setUpdatedAt(new \DateTime('now'));
    }

    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets the value of reference.
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Sets the value of reference.
     *
     * @param string $reference the reference
     *
     * @return self
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

     /**
     * @param proposal[] $proposals
     */
    public function setProposals($proposals)
    {
        $this->proposals = new ArrayCollection();

        foreach ($proposals as $proposal) {
            $this->addProposal($proposal);
        }

    }

    /**
     * Add proposals
     *
     * @param \MainBundle\Entity\proposal $proposal
     * @return Role
     */
    public function addProposal(Proposal $proposal)
    {
        $proposal->setResponse($this);
        $this->proposals[] = $proposal;

        return $this;
    }

    /**
     * Remove proposals
     *
     * @param \MainBundle\Entity\proposal $proposal
     */
    public function removeProposal(Proposal $proposal)
    {
        $this->proposals->removeElement($proposal);
        $this->setProposals($this->proposals);
    }

    /**
     * Get proposals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProposals()
    {
        return $this->proposals;
    }

    /**
     * Gets the value of offer.
     *
     * @return mixed
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * Sets the value of offer.
     *
     * @param mixed $offer the offer
     *
     * @return self
     */
    public function setOffer(Offer $offer)
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * Gets the value of lastStep.
     *
     * @return integer
     */
    public function getLastStep()
    {
        return $this->lastStep;
    }

    /**
     * Sets the value of lastStep.
     *
     * @param integer $lastStep the last step
     *
     * @return self
     */
    public function setLastStep($lastStep)
    {
        $this->lastStep = $lastStep;

        return $this;
    }

    /**
     * Gets the value of extraInfo.
     *
     * @return mixed
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * Sets the value of extraInfo.
     *
     * @param mixed $extraInfo the extra info
     *
     * @return self
     */
    public function setExtraInfo($extraInfo)
    {
        $this->extraInfo = $extraInfo;

        return $this;
    }

    /**
     * Gets the value of user.
     *
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets the value of user.
     *
     * @param mixed $user the user
     *
     * @return self
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
}