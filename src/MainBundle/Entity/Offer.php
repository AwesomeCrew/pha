<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Offre
 *
 * @ORM\Table(name="offers")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\OfferRepository")
 * @DoctrineAssert\UniqueEntity("id")
 *
 * @author Maior Valentin <maior.valentin@gmail.com>
 * @copyright 2015 si0n
 */
class Offer {

    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\Column(type="string", name="reference")
     * @var string
     */
    private $reference;

    /**
     * @ORM\Column(type="string", name="duration")
     * @var string
     */
    private $duration;

    /**
     * @ORM\Column(type="string", name="year")
     * @var string
     */
    private $year;

    /**
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\Content", cascade={"persist", "remove"}, mappedBy="offer")
     * @Assert\Valid
     */
    private $contents;

    /**
     * @ORM\OneToMany(targetEntity="MessageBundle\Entity\Thread", cascade={"persist", "remove"}, mappedBy="offer")
     * @Assert\Valid
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\Response", cascade={"persist", "remove"}, mappedBy="offer")
     * @Assert\Valid
     */
    private $responses;

    /** @ORM\Column(type="boolean") */
    private $validated;

    /**
     * @ORM\Column(type="boolean", name="is_read")
     * @var bool
     */
    private $read = false;

    /**
     * @ORM\Column(type="boolean", name="completed")
     * @var bool
     */
    private $completed = false;

    /**
     * @ORM\Column(type="integer", name="winner", nullable=true)
     * @var bool
     */
    private $winner;

    /**
     * @return boolean
     */
    public function isCompleted() {
        return $this->completed;
    }

    /**
     * @param boolean $completed
     */
    public function setCompleted( $completed ) {
        $this->completed = $completed;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->responses = new ArrayCollection();
        $this->contents = new ArrayCollection();
        $this->setCreatedAt(new \DateTime('now'));
        $this->setUpdatedAt(new \DateTime('now'));
    }

    public function __toString()
    {
        return 'OfferEntity';
    }

    /**
     * Gets the value of reference.
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Sets the value of reference.
     *
     * @param string $reference the reference
     *
     * @return self
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Gets the value of duration.
     *
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Sets the value of duration.
     *
     * @param string $duration the duration
     *
     * @return self
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Gets the value of year.
     *
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Sets the value of year.
     *
     * @param string $year the year
     *
     * @return self
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param content[] $contents
     */
    public function setContents($contents)
    {
        $this->contents = new ArrayCollection();

        foreach ($contents as $content) {
            $this->addContent($content);
        }

    }

    /**
     * Add contents
     *
     * @param \MainBundle\Entity\content $content
     * @return Role
     */
    public function addContent(Content $content)
    {
        $content->setOffer($this);
        $this->contents[] = $content;

        return $this;
    }

    /**
     * Remove contents
     *
     * @param \MainBundle\Entity\content $content
     */
    public function removeContent(Content $content)
    {
        $this->contents->removeElement($content);
        $this->setContents($this->contents);
    }

    /**
     * Get contents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * Gets the value of read.
     *
     * @return mixed
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * Sets the value of read.
     *
     * @param mixed $read the read
     *
     * @return self
     */
    public function setRead($read)
    {
        $this->read = $read;

        return $this;
    }

    /**
     * Gets the value of validated.
     *
     * @return mixed
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Sets the value of validated.
     *
     * @param mixed $validated the validated
     *
     * @return self
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * @param content[] $responses
     */
    public function setResponses($responses)
    {
        $this->responses = new ArrayCollection();

        foreach ($responses as $response) {
            $this->addResponse($response);
        }

    }

    /**
     * Add responses
     *
     * @param \MainBundle\Entity\Response $response
     * @return Role
     */
    public function addResponse(Response $response)
    {
        $response->setOffer($this);
        $this->responses[] = $response;

        return $this;
    }

    /**
     * Remove responses
     *
     * @param \MainBundle\Entity\Response $response
     */
    public function removeResponse(Response $response)
    {
        $this->responses->removeElement($response);
        $this->setResponses($this->responses);
    }

    /**
     * Get responses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * Gets the value of messages.
     *
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Sets the value of messages.
     *
     * @param mixed $messages the messages
     *
     * @return self
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getWinner() {
        return $this->winner;
    }

    /**
     * @param boolean $winner
     *
     * @return $this
     */
    public function setWinner( $winner ) {
        $this->winner = $winner;

        return $this;
    }


}