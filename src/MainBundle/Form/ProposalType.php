<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProposalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prix', 'text', ['label' => 'Prix', 'attr' => ['placeholder' => 'Prix']])
            ->add('volume', 'text', ['label' => 'Volume', 'attr' => ['placeholder' => 'Volume']])
            ->add('productId', 'text')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Proposal'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mainbundle_proposal';
    }
}
