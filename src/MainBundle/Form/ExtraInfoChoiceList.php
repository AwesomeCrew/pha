<?php

namespace MainBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\ChoiceList\LazyChoiceList;

class ExtraInfoChoiceList extends LazyChoiceList {

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     * @param $aRoles
     */
    public function __construct(EntityManager $em){
        $this->em = $em;
    }

    /**
     *
     */
    protected function loadChoiceList()
    {
        $infos = $this->em->getRepository('MainBundle:ExtraInfo')->getAllForSelect();

        return new ChoiceList($infos[0], $infos[1]);
    }
}