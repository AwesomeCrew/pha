<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MemberType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', 'text', ['label' => 'Prénom', 'attr' => ['placeholder' => 'Prénom']])
            ->add('last_name', 'text', ['label' => 'Nom', 'attr' => ['placeholder' => 'Nom']])
            ->add('email', 'text', ['label' => 'E-mail', 'attr' => ['placeholder' => 'E-mail']])
            ->add('phone', 'text', ['label' => 'Numéro de mobile', 'attr' => ['placeholder' => 'Numéro de mobile']])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mainbundle_member';
    }
}
