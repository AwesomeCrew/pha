<?php

namespace MainBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OfferType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reference', 'text', ['label' => 'Référence'])
            ->add('duration', 'text', ['label' => 'Durée de l\'appel d\'offre'])
            ->add('year', 'text', ['label' => 'Année'])
            ->add('validated', 'checkbox', ['label' => 'Validé', 'required' => false])
            ->add('contents',
                    'bootstrap_collection',
                    array(
                        'type'               => new ContentType(),
                        'allow_add'          => true,
                        'allow_delete'       => true,
	                    'label'              => 'Produits',
                        'add_button_text'    => '',
                        'delete_button_text' => '',
                        'sub_widget_col'     => 10,
                        'button_col'         => 2,
                        'attr'               => array('colspread' => 10),
                        'by_reference'       => false
                    )
                )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Offer'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mainbundle_offer';
    }
}
