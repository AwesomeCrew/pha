<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;

class ResponseType extends AbstractType
{

    private $em;
    private $preselectedValue;

    public function __construct(EntityManager $em, $preselectedValue=''){
        $this->em = $em;
        $this->preselectedValue = $preselectedValue;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reference', 'text', ['label' => 'Référence'])
            ->add('lastStep', 'text')
            ->add('extraInfo', 'choice', array(
                    'choice_list' => new ExtraInfoChoiceList($this->em),
                    'data' => $this->preselectedValue
                    ))
            ->add('proposals',
                    'bootstrap_collection',
                    array(
                        'type'               => new ProposalType(),
                        'allow_add'          => true,
                        'allow_delete'       => true,
                        'label'              => 'Proposals',
                        'add_button_text'    => '',
                        'delete_button_text' => '',
                        'sub_widget_col'     => 10,
                        'button_col'         => 2,
                        'attr'               => array('colspread' => 10),
                        'by_reference'       => false
                    )
                )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Response',
            'csrf_protection' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mainbundle_response';
    }
}
