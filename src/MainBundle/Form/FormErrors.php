<?php

namespace MainBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Translation\TranslatorInterface;


class FormErrors
{
    private $translator;

    /**
     * @param \Symfony\Component\Form\Form $form
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param \Symfony\Component\Form\Form $form
     * @return array
     */
    public function getFormErrors(Form $form)
    {
        $errors = array();
        if ($err = $this->childErrors($form)) {
            $errors["form"] = $err;
        }

        foreach ($form->all() as $key => $child) {
            //
            if ($err = $this->childErrors($child)) {
                $errors[$key] = $err;
            }
        }

        return $errors;
    }

    /**
     * @param \Symfony\Component\Form\Form $form
     * @return array
     */
    public function childErrors(Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $error) {
            //@todo: create translation for custom error
            //$message = $this->translator->trans($error->getMessage(), array(), 'validators');
            array_push($errors, $error->getMessage());
        }

        return $errors;
    }
}