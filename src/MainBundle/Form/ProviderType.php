<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProviderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', ['label' => 'Nom'])
            ->add('reference', 'text', ['label' => 'Référence'])
            ->add('social_reason', 'text', ['label' => 'Raison Sociale'])
            ->add('siret', 'text', ['label' => 'Siret'])
            ->add('address', 'text', ['label' => 'Adresse'])
            ->add('zipcode', 'number', ['label' => 'Code postal'])
            ->add('country', 'text', ['label' => 'Pays', 'data' => 'France'])
            ->add('phone', 'text', ['label' => 'Téléphone'])
            ->add('users',
                    'bootstrap_collection',
                    array(
                        'type'               => new MemberType(),
                        'allow_add'          => true,
                        'allow_delete'       => true,
	                    'label'              => 'Utilisateurs',
                        'add_button_text'    => '',
                        'delete_button_text' => '',
                        'sub_widget_col'     => 10,
                        'button_col'         => 2,
                        'attr'               => array('colspread' => 10),
                        'by_reference'       => false
                    )
                )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Provider'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mainbundle_provider';
    }
}
