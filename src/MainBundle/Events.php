<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MainBundle;

/**
 * Contains all events thrown in the FOSUserBundle
 */
final class Events
{
    /**
     * The CALENDAR_EVENT_COMPLETED event occurs when a calendar event has been successfully added
     *
     * This event allows you to modify the default values of the user before binding the form.
     * The event listener method receives a FOS\UserBundle\Event\GetResponseUserEvent instance.
     */
    const CALENDAR_EVENT_COMPLETED = 'herbadoc.calendar_event.completed';

}
