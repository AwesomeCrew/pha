<?php

namespace MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

use MainBundle\Form\OfferType;
use MainBundle\Entity\Offer;
use UserBundle\Entity\User;
use MainBundle\Repository\OfferRepository;
use MainBundle\Repository\ContentRepository;

class OfferController extends Controller
{
    public function listingAction()
    {

        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();

        if (in_array(User::ROLE_FRNS, $user->getRoles())) {
            $offers = $em->getRepository('MainBundle:Offer')->findBy(array('validated' => true));
            $responses = $em->getRepository('MainBundle:Response')->findByUser($user);
            foreach ($responses as $response) {
                $key = array_search($response->getOffer(), $offers, true);

                if ($key === false || $response->getLastStep() < 5 ) {
                    continue;
                }

                unset($offers[$key]);
            }
        } elseif (in_array(User::ROLE_EDF, $user->getRoles())) {
            $offers = $em->getRepository('MainBundle:Offer')->findAll();
        }

        return $this->render('MainBundle:Offer:listing.html.twig', array('offers' => $offers));
    }

    public function listAnsweredAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $responses = $em->getRepository('MainBundle:Response')->findByUser($user);
        $offers = [];
        foreach ($responses as $response) {
            if ($response->getLastStep() == 5) {
                $offers[] = $response->getOffer();
            }
        }

        return $this->render('MainBundle:Offer:listing.html.twig', array('offers' => $offers, 'repondus' => true));
    }

    /**
     * Creates a new Offer entity.
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function creationAction(Request $request)
    {
        $entity = new Offer();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            //If over is valide send email

            return $this->redirect($this->generateUrl('main_offer_listing', array('id' => $entity->getId())));
        }

        return $this->render('MainBundle:Offer:creation.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Offer entity.
     *
     * @param Offer $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Offer $entity)
    {
        $form = $this->createForm(new OfferType($this->getDoctrine()->getManager()), $entity, array(
            'action' => $this->generateUrl('main_offer_creation'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Sauvegarder'));
        $form->add('button', 'button', array('label' => 'Annuler'));

        return $form;
    }

    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MainBundle:Offer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('MainBundle:Offer:creation.html.twig', array(
            'entity' => $entity,
            'form'   => $editForm->createView(),
        ));
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MainBundle:Offer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('MainBundle:Offer:show.html.twig', array(
            'entity' => $entity,
            'form'   => $editForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Offer entity.
     *
     * @param Offer $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Offer $entity)
    {
        $form = $this->createForm(new OfferType($this->getDoctrine()->getManager()), $entity, array(
            'action' => $this->generateUrl('main_offer_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modifier'));
        $form->add('button', 'button', array('label' => 'Retour à la liste'));

        return $form;
    }

    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MainBundle:Offer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        //$deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('main_offer_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView()
        );
    }

    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Offer')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Offer entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('main_offer_listing'));
    }

    private function sendEmailToProviders($offer)
    {

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function followAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $average = $em->getRepository('MainBundle:Proposal')->getAveragePriceByProduct($id);
        $offer = $em->getRepository('MainBundle:Offer')->find($id);
        $products = [];
        foreach ($offer->getResponses() as $resp){
            foreach ($resp->getProposals() as $props) {
                $products[$props->getProduct()->getProduct()->getName()]['prix'] =
                    isset($products[$props->getProduct()->getProduct()->getName()]['prix']) ?
                        $products[$props->getProduct()->getProduct()->getName()]['prix'] + $props->getPrix() :
                        $props->getPrix();
                $products[$props->getProduct()->getProduct()->getName()]['count'] =
                    isset($products[$props->getProduct()->getProduct()->getName()]['count']) ?
                        $products[$props->getProduct()->getProduct()->getName()]['count'] + 1 :
                        1;
            }
        }
        foreach ( $offer->getContents() as $content ) {
            $products[$content->getProduct()->getName()]['volume'] = $content->getVolume();
        }

        if (!$offer) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $editForm = $this->createEditForm($offer);

        return $this->render('MainBundle:Offer:follow.html.twig', [
            'offer' => $offer,
            'average' => $average,
            'products' => $products,
            'form'   => $editForm->createView()
        ]);
    }

    /**
     * @param int $provider
     * @param int $offer
     *
     * @return JsonResponse
     */
    public function closeAction($provider, $offer){

        try {
            $em = $this->getDoctrine()->getManager();

            /**
             * @var \MainBundle\Entity\Offer
             */
            $offer = $em->getRepository('MainBundle:Offer')->find($offer);
            $offer->setCompleted(true);
            $offer->setWinner($provider);

            $em->persist($offer);
            $em->flush();
        } catch (Exception $e){

        }
        return new JsonResponse();
    }
}
