<?php

namespace MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Common\Collections\ArrayCollection;

use MainBundle\Form\ResponseType;
use MainBundle\Entity\Response;
use MainBundle\Repository\ResponseRepository;
use MainBundle\Repository\ProposalRepository;
use MainBundle\Repository\ContentRepository;
use MainBundle\Repository\OfferRepository;
use MainBundle\Form\OfferType;
use MainBundle\Entity\Proposal;
use MainBundle\Entity\Content;
use MainBundle\Entity\Offer;


use MainBundle\Mailer\Mailer;

class ResponseController extends Controller
{
    public function createAction(Request $request, $offer, $user)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MainBundle:Response')->findOneBy(array('user' => $user, 'offer' => $offer));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Response entity.');
        }



        $form = $this->createForm(new ResponseType($em), $entity);
        $form->handleRequest($request);

        $offer = $em->getRepository('MainBundle:Offer')->find($offer);
        $entity->setOffer($offer);

        if ($entity->getExtraInfo() != 0) {
            $extraInfo = $em->getRepository('MainBundle:ExtraInfo')->findOneById($entity->getExtraInfo());
            $entity->setExtraInfo($extraInfo);
        }

        $proposals = $entity->getProposals();
        foreach ($proposals as $proposal) {
            $productProxy = $em->getRepository('MainBundle:Content')->find($proposal->getProduct()->getId());
            $proposal->setProduct($productProxy);
        }

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();
        } else {
            $errors = $this->get('form_errors')->getFormErrors($form);
            die(var_dump($errors));
        }


        $this->container->get('session')->getFlashBag()->add('success', 'Merci pour votre réponse, elle a été prise en compte.');

        return new JsonResponse(json_encode(array('succes' => true)));
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $offer = $em->getRepository('MainBundle:Offer')->find($id);
        $user = $this->get('security.context')->getToken()->getUser();
        $response = $em->getRepository('MainBundle:Response')->findOneBy(array('user' => $user->getId(), 'offer' => $offer));

        if (!$response) {
            throw $this->createNotFoundException('Unable to find Response entity with this id.');
        }

        $form = $this->createForm(new ResponseType($em, $response->getExtraInfo()->getId()), $response, array());

        return $this->render('MainBundle:Response:show.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $offer = $em->getRepository('MainBundle:Offer')->find($id);

        if (!$offer) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $user = $this->get('security.context')->getToken()->getUser();
        $response = $em->getRepository('MainBundle:Response')->findOneBy(array('user' => $user->getId(), 'offer' => $offer));

        if ($response === null) {

            $response = new Response();
            $response->setReference($offer->getId());
            $response->setLastStep(0);
            $proposals = new ArrayCollection();

            foreach ($offer->getContents() as $content) {
                $proposal = new Proposal();
                $proposal->setResponse($response);
                $proposal->setPrix(0);
                $proposal->setVolume(0);
                $proposal->setProduct($content);
                $proposals->add($proposal);
            }

            $response->setProposals($proposals);
            $em->persist($response);
            $em->flush();
        }

        $preselectedValue = ($response->getExtraInfo() !== null) ? $response->getExtraInfo()->getId() : 0;
        $responseForm = $this->createForm(new ResponseType($em, $preselectedValue), $response, array());

        $offerForm = $this->createForm(new OfferType(), $offer, array());

        return $this->render('MainBundle:Response:view.html.twig', array(
            'offer' => $offer,
            'offerForm'    => $offerForm->createView(),
            'responseForm' => $responseForm->createView(),
            'lastStep' => $response->getLastStep()
        ));
    }

    public function updateStepAction($offer, $step)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MainBundle:Response')->findOneBy(array('user' => $user->getId(), 'offer' => $offer));

        if ($entity !== null) {
            $entity->setLastStep($step);
        }

        $em->persist($entity);
        $em->flush();

        if ($step === '5') {
            //send in app message
            $composer = $this->container->get('fos_message.composer');
            $destEDF = $em->getRepository('UserBundle:User')->findOneBy(array('id' => $this->container->getParameter('main_edf_user_id')));


            $message = $composer->newThread()
                ->setSender($user)
                ->addRecipient($destEDF)
                ->setSubject('Hi there')
                ->setBody('Just sent a new response')
                //->setOffer($entity->getOffer())
                ->getMessage();

            $sender = $this->container->get('fos_message.sender');
            $sender->send($message);

            //send email and redirect to lists
            //Mailer::sendNewOfferEmailMessage($entity);

             $this->container->get('session')->getFlashBag()->add('success', 'Votre validation a été prise en compte. ');

            return new JsonResponse(json_encode(array('succes' => true, 'redirect' => $this->generateUrl('main_offer_listing'))));
        }

        return new JsonResponse(json_encode(array('succes' => true)));
    }
}