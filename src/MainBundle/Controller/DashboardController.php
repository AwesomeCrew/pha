<?php
/**
 *
 * @package     MainBundle\Controller
 * @category    pha
 * @author      Bogdan SOOS <bogdan.soos@dynweb.org>
 * @copyright   2015 - 2015 pha
 */

namespace MainBundle\Controller;


use MessageBundle\Entity\Message;
use MainBundle\Entity\Provider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class DashboardController.
 *
 * @package MainBundle\Controller
 */
class DashboardController extends Controller {

	public function indexAction()
	{
		$provider = $this->container->get('fos_message.provider');
		$this->container->get('session')->set('unreadNumberOfMessages', $provider->getNbUnreadMessages());
		$threads = $provider->getInboxThreads();
		$user = $this->get('security.context')->getToken()->getUser();
		$em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $responses = $em->getRepository('MainBundle:Response')->findByUser($user);
        $responses = $em->getRepository('MainBundle:Response')->findByUser($user);
        $myOffers = [];
        foreach ($responses as $response) {
            if ($response->getLastStep() == 5) {
                $myOffers[] = $response->getOffer();
            }
        }

		$allOffers = $em->getRepository('MainBundle:Offer')->findBy(array('validated' => true));
            $responses = $em->getRepository('MainBundle:Response')->findByUser($user);
            foreach ($responses as $response) {
                $key = array_search($response->getOffer(), $allOffers, true);

                if ($key === false || $response->getLastStep() < 5 ) {
                    continue;
                }

                unset($allOffers[$key]);
            }

		return $this->render('MainBundle:Dashboard:index.html.twig', [
			'roles'     => $this->getUser()->getRoles(),
			'role_edf'  => Provider::ROLE_EDF,
			'role_frns' => Provider::ROLE_FRNS,
			'threads'   => $threads,
			'allOffers' => $allOffers,
			'myOffers' => $myOffers
		]);
	}
}