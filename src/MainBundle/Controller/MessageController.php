<?php

namespace MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class MessageController extends Controller
{
    public function inboxAction()
    {
        return $this->render('MainBundle:Message:inbox.html.twig', array());
    }

    public function composeAction()
    {
        return $this->render('MainBundle:Message:compose.html.twig', array());
    }

    public function detailAction()
    {
        return $this->render('MainBundle:Message:detail.html.twig', array());
    }
}