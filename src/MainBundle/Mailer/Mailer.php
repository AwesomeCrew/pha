<?php

namespace MainBundle\Mailer;

use FOS\UserBundle\Mailer\Mailer as BaseMailer;

use MainBundle\Entity\Response;

class Mailer extends BaseMailer
{

    /**
     * {@inheritdoc}
     */
    public static function sendNewOfferEmailMessage(Response $response)
    {
        $template = 'MainBundle:Mail:new_response.txt.twig';
        $rendered = $this->templating->render($template, array());
        $from = array("robot@si0n.com" => "Si0n - Robot");
        $to = $response->getUser()->getEmail();
        $this->sendEmailMessage($rendered, $from, 'v.maior@si0n.com');
    }
}