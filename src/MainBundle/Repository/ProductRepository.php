<?php
/**
 * This file is part of the Pilotage package.
 *
 * @author Bogdan SOOS <bogdan.soos@external.gdfsuez.com>
 * @created 07/07/15 15:57
 * @version 1.0
 */

namespace MainBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ProductRepository.
 *
 * @package MainBundle\Repository
 */
class ProductRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getAllForSelect()
    {
        $all = $this->findAll();

        $ids = [];
        $infos = [];
        foreach ($all as $info) {
            $ids[] = $info->getId();
            $infos[] = $info->getName();
        }

        return array($ids, $infos);
    }
}