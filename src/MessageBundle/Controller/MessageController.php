<?php
/**
 *
 * @package MessageBundle\Controller
 * @category    pha
 * @author      Bogdan SOOS <bogdan.soos@dynweb.org>
 * @copyright   2015 - 2015 pha
 * @version     SVN: $Id$
 *
 */

namespace MessageBundle\Controller;

use MessageBundle\Entity\NewThreadMultipleMessage;
use FOS\MessageBundle\Provider\ProviderInterface;
use MessageBundle\Form\Type\NewMultiMessageFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class MessageController extends Controller implements ContainerAwareInterface {


	/**
	 * @var ContainerInterface
	 *
	 * @api
	 */
	protected $container;

	/**
	 * Index action.
	 */
	public function indexAction(){
		return $this->container->get('templating')->renderResponse('MessageBundle:Message:index.html.twig');
	}

	/**
	 * Displays the authenticated participant messages sent
	 *
	 * @return Response
	 */
	public function sentAction()
	{
		$threads = $this->getProvider()->getSentThreads();

		return $this->container->get('templating')->renderResponse('MessageBundle:Message:inbox.html.twig', array(
			'threads' => $threads
		));
	}

	/**
	 * Displays the authenticated participant inbox
	 *
	 * @return Response
	 */
	public function inboxAction()
	{
		$threads = $this->getProvider()->getInboxThreads();

		return $this->container->get('templating')->renderResponse('MessageBundle:Message:inbox.html.twig', array(
			'threads' => $threads
		));
	}

	/**
	 *
	 */
	public function composeAction(){
		$thread = new NewThreadMultipleMessage();;
		$form = $this->createForm(new NewMultiMessageFormType($this->getDoctrine()->getManager(), $this->getUser()->getRoles()), $thread);

		$formHandler = $this->container->get('message_bundle_new_thread_multiple_form');
		if ($message = $formHandler->process($form)) {
			return new RedirectResponse($this->container->get('router')->generate('main_message_detail', array(
				'threadId' => $message->getThread()->getId()
			)));
		}

		return $this->container->get('templating')->renderResponse('MessageBundle:Message:compose.html.twig', array(
			'form' => $form->createView(),
			'data' => $form->getData()
		));
	}

	/**
	 * Displays a thread, also allows to reply to it
	 *
	 * @param string $threadId the thread id
	 *
	 * @return Response
	 */
	public function detailAction($threadId)
	{
		$thread = $this->getProvider()->getThread($threadId);
		$form = $this->container->get('fos_message.reply_form.factory')->create($thread);
		//$form = $this->createForm(new ReplyMessageFormType(), $thread);
		$formHandler = $this->container->get('fos_message.reply_form.handler');

		if ($message = $formHandler->process($form)) {
			return new RedirectResponse($this->container->get('router')->generate('main_message_detail', array(
				'threadId' => $message->getThread()->getId()
			)));
		}

		$provider = $this->container->get('fos_message.provider');
		$this->container->get('session')->set('unreadNumberOfMessages', $provider->getNbUnreadMessages());

		return $this->container->get('templating')->renderResponse('MessageBundle:Message:detail.html.twig', array(
			'form' => $form->createView(),
			'thread' => $thread
		));
	}


	/**
	 * Gets the provider service
	 *
	 * @return ProviderInterface
	 */
	protected function getProvider()
	{
		return $this->container->get('fos_message.provider');
	}
}