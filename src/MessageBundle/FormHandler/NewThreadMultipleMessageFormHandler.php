<?php
/**
 *
 * @package MessageBundle\FormHandler
 * @category    pha
 * @author      Bogdan SOOS <bogdan.soos@dynweb.org>
 * @copyright   2015 - 2015 pha
 * @version     SVN: $Id$
 *
 */

namespace MessageBundle\FormHandler;


use FOS\MessageBundle\FormHandler\AbstractMessageFormHandler;
use FOS\MessageBundle\FormModel\AbstractMessage;
use FOS\MessageBundle\Model\MessageInterface;
use MessageBundle\Entity\NewThreadMultipleMessage;

class NewThreadMultipleMessageFormHandler extends AbstractMessageFormHandler {


	/**
	 * Composes a message from the form data
	 *
	 * @param AbstractMessage $message
	 *
	 * @return MessageInterface the composed message ready to be sent
	 * @throws \InvalidArgumentException if the message is not a NewThreadMessage
	 */
	public function composeMessage(AbstractMessage $message)
	{
		if (!$message instanceof NewThreadMultipleMessage) {
			throw new \InvalidArgumentException(sprintf('Message must be a NewThreadMultipleMessage instance, "%s" given', get_class($message)));
		}


		return $this->composer->newThread()
		                      ->setSubject($message->getSubject())
		                      ->addRecipients($message->getRecipients())
		                      ->setSender($this->getAuthenticatedParticipant())
							  ->setOffer($message->getOffer())
		                      ->setBody($message->getBody())
		                      ->getMessage();
	}

}