<?php
/**
 *
 * @package MessageBundle\Form\Type
 * @category    pha
 * @author      Bogdan SOOS <bogdan.soos@dynweb.org>
 * @copyright   2015 - 2015 pha
 * @version     SVN: $Id$
 *
 */

namespace MessageBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReplyMessageFormType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('body', 'textarea');
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'intention'  => 'reply',
		));
	}

	public function getName()
	{
		return 'edf_message_reply_message';
	}
}
