<?php
/**
 *
 * @package MessageBundle\Form\Type
 * @category    pha
 * @author      Bogdan SOOS <bogdan.soos@dynweb.org>
 * @copyright   2015 - 2015 pha
 * @version     SVN: $Id$
 *
 */

namespace MessageBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewMessageFormType extends AbstractType {


	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('recipient', 'fos_user_username')
			->add('subject', 'text')
			->add('body', 'textarea', ['required' => false]);
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'intention'  => 'message',
		));
	}


	/**
	 * Returns the name of this type.
	 *
	 * @return string The name of this type
	 */
	public function getName() {
		return 'edf_new_message';
	}
}