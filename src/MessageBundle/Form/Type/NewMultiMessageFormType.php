<?php
/**
 *
 * @package MessageBundle\Form\Type
 * @category    pha
 * @author      Bogdan SOOS <bogdan.soos@dynweb.org>
 * @copyright   2015 - 2015 pha
 * @version     SVN: $Id$
 *
 */

namespace MessageBundle\Form\Type;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use UserBundle\Repository\UserChoiceList;

class NewMultiMessageFormType extends AbstractType {

	/**
	 * @var array
	 */
	private $em;

	/**
	 * @var array
	 */
	private $aRoles = [];

	/**
	 * @param EntityManager $em
	 * @param $aRoles
	 */
	public function __construct(EntityManager $em, $aRoles){
		$this->em = $em;
		$this->aRoles = $aRoles;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('recipients', 'choice', [
				'choice_list' => new UserChoiceList($this->em, $this->aRoles),
				'multiple' => true,
			])
			->add('subject', 'text')
			->add('offer', 'entity', [
				'class' => 'MainBundle:Offer',
				'property' => 'reference'
			])
			->add('body', 'textarea', ['required' => false]);
	}

	public function getName()
	{
		return 'edf_new_message';
	}
}