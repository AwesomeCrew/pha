/**
 * Created by Bogdan SOOS <bogdan.soos@dynweb.org>.
 */

(function( Edf, $, undefined ) {

    /**
     * Log text to console, avoid IE JS Errors
     *
     * @param sText
     */
    Edf.console = function(sText){
        if (console && console.log){
            console.log(sText);
        }
    };

    /**
     * Public Method
     */
    Edf.loadInbox = function() {alert("!");
        jQuery('a[id=composeEmailMessage]').off().on('click', function(e){
            e.preventDefault();
            var sUrl = jQuery(this).data('url');
            var sResponseDiv = jQuery(this).data('response');
            if ('' != sUrl){
                jQuery.ajax({
                    type    : 'post',
                    url     : sUrl,
                    dataType: 'html',
                    success : function (sResponse) {
                        sResponseDiv.html(sResponse);
                    }
                })
            } else {
                return false;
            }
        })
    };

}( window.Edf = window.Edf || {}, jQuery ));


/**
 * Go baby.
 */
jQuery(document).ready(function(){alert("asdasd");
    Edf.loadInbox();

    /**
     * First time we load the page.
     */
    jQuery('a[id=composeEmailMessage]').trigger('click');
});