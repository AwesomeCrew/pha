<?php
/**
 *
 * @package MessageBundle\Entity
 * @category    pha
 * @author      Bogdan SOOS <bogdan.soos@dynweb.org>
 * @copyright   2015 - 2015 pha
 * @version     SVN: $Id$
 *
 */

namespace MessageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\MessageBundle\FormModel\AbstractMessage;
use FOS\MessageBundle\Model\ParticipantInterface;
use MainBundle\Entity\Offer;

class NewThreadMultipleMessage extends AbstractMessage {

	/**
	 * The user who receives the message
	 *
	 * @var Array
	 */
	protected $recipients;

	/**
	 * The thread subject
	 *
	 * @var string
	 */
	protected $subject;

	/**
	 * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Offer")
	 * @var \MainBundle\Entity\Offer
	 */
	protected $offer;


	/**
	 * @return string
	 */
	public function getSubject()
	{
		return $this->subject;
	}

	/**
	 * @param string $subject
	 *
	 * @return null
	 */
	public function setSubject($subject)
	{
		$this->subject = $subject;
	}

	/**
	 * @return Array
	 */
	public function getRecipients()
	{
		if ($this->recipients === null) {
			$this->recipients = [];//new ArrayCollection();
		}

		return $this->recipients;
	}

	/**
	 * Adds single recipient to collection
	 *
	 * @param ParticipantInterface $recipient
	 *
	 * @return null
	 */
	public function addRecipient($recipient)
	{
		/*if (!$this->recipients->contains($recipient)) {
			$this->recipients->add($recipient);
		}*/

		if (!in_array($recipient, $this->recipients)){
			$this->recipients[] = $recipient;
		}
	}


	/**
	 * Removes recipient from collection
	 *
	 * @param ParticipantInterface $recipient
	 *
	 * @return null
	 *
	 */
	public function removeRecipient( $recipient)
	{
		$key = array_search($recipient, $this->recipients, true);

		if ($key === false) {
			return false;
		}

		unset($this->recipients[$key]);
	}

	/**
	 * Set all recipients at once.
	 *
	 * @param $recipients
	 */
	public function setRecipients($recipients){
		$this->recipients = $recipients;
	}


	/**
	 * @return ArrayCollection
	 */
	public function getOffer()
	{
		return $this->offer;
	}

	/**
	 * Adds single recipient to collection
	 *
	 * @param Offer $offer
	 *
	 * @return null
	 */
	public function setOffer($offer)
	{
		$this->offer = $offer;
	}
}