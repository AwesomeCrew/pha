<?php
/**
 *
 * @package MessageBundle\Entity
 * @category    pha
 * @author      Bogdan SOOS <bogdan.soos@dynweb.org>
 * @copyright   2015 - 2015 pha
 * @version     SVN: $Id$
 *
 */

namespace MessageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\MessageBundle\Entity\MessageMetadata as BaseMessageMetadata;

/**
 * Class MessageMetadata.
 *
 * @package MessageBundle\Entity
 * @ORM\Entity
 */
class MessageMetadata extends BaseMessageMetadata {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\ManyToOne(
	 *   targetEntity="MessageBundle\Entity\Message",
	 *   inversedBy="metadata"
	 * )
	 * @var \FOS\MessageBundle\Model\MessageInterface
	 */
	protected $message;

	/**
	 * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
	 * @var \FOS\MessageBundle\Model\ParticipantInterface
	 */
	protected $participant;
}