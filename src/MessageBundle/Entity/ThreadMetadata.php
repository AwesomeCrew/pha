<?php
/**
 *
 * @package MessageBundle\Entity
 * @category    pha
 * @author      Bogdan SOOS <bogdan.soos@dynweb.org>
 * @copyright   2015 - 2015 pha
 * @version     SVN: $Id$
 *
 */

namespace MessageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\MessageBundle\Entity\ThreadMetadata as BaseThreadMetadata;


/**
 * Class ThreadMetadata
 * @package MessageBundle\Entity
 * @ORM\Entity
 */
class ThreadMetadata extends BaseThreadMetadata
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\ManyToOne(
	 *   targetEntity="MessageBundle\Entity\Thread",
	 *   inversedBy="metadata"
	 * )
	 * @var \FOS\MessageBundle\Model\ThreadInterface
	 */
	protected $thread;

	/**
	 * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
	 * @var \FOS\MessageBundle\Model\ParticipantInterface
	 */
	protected $participant;
}