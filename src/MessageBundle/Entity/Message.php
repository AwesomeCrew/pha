<?php
/**
 * @package     MessageBundle\Entity
 * @category    pha
 * @author      Bogdan SOOS <bogdan.soos@dynweb.org>
 * @copyright   2015 - 2015 pha
 * @version     SVN: $Id$
 *
 */

namespace MessageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\MessageBundle\Entity\Message as BaseMessage;

/**
 * Class Message
 * @package MessageBundle\Entity
 * @ORM\Entity
 */
class Message extends BaseMessage {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\ManyToOne(
	 *   targetEntity="MessageBundle\Entity\Thread",
	 *   inversedBy="messages"
	 * )
	 * @var \FOS\MessageBundle\Model\ThreadInterface
	 */
	protected $thread;

	/**
	 * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
	 * @var \FOS\MessageBundle\Model\ParticipantInterface
	 */
	protected $sender;

	/**
	 * @ORM\OneToMany(
	 *   targetEntity="MessageBundle\Entity\MessageMetadata",
	 *   mappedBy="message",
	 *   cascade={"all"}
	 * )
	 * @var MessageMetadata[]|\Doctrine\Common\Collections\Collection
	 */
	protected $metadata;

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId( $id ) {
		$this->id = $id;
	}

	/**
	 * @return \FOS\MessageBundle\Model\ThreadInterface
	 */
	public function getThread() {
		return $this->thread;
	}

	/**
	 * @param \FOS\MessageBundle\Model\ThreadInterface $thread
	 */
	public function setThread(\FOS\MessageBundle\Model\ThreadInterface $thread ) {
		$this->thread = $thread;
	}

	/**
	 * @return \FOS\MessageBundle\Model\ParticipantInterface
	 */
	public function getSender() {
		return $this->sender;
	}

	/**
	 * @param \FOS\MessageBundle\Model\ParticipantInterface $sender
	 */
	public function setSender(\FOS\MessageBundle\Model\ParticipantInterface $sender ) {
		$this->sender = $sender;
	}

	/**
	 * @return \Doctrine\Common\Collections\Collection|MessageMetadata[]
	 */
	public function getMetadata() {
		return $this->metadata;
	}

	/**
	 * @param \Doctrine\Common\Collections\Collection|MessageMetadata[] $metadata
	 */
	public function setMetadata( $metadata ) {
		$this->metadata = $metadata;
	}



}