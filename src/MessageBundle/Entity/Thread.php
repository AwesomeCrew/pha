<?php
/**
 *
 * @package MessageBundle\Entity
 * @category    pha
 * @author      Bogdan SOOS <bogdan.soos@dynweb.org>
 * @copyright   2015 - 2015 pha
 * @version     SVN: $Id$
 *
 */

namespace MessageBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\MessageBundle\Entity\DateTime;
use FOS\MessageBundle\Entity\Thread as BaseThread;
use FOS\MessageBundle\Model\ParticipantInterface;
use MainBundle\Entity\Offer;
use UserBundle\Entity\User;

/**
 * Class Thread
 * @package MessageBundle\Entity
 * @ORM\Entity
 */
class Thread extends BaseThread {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
	 * @var User
	 */
	protected $createdBy;

	/**
	 * @ORM\OneToMany(
	 *   targetEntity="MessageBundle\Entity\Message",
	 *   mappedBy="thread"
	 * )
	 * @var Message[]|\Doctrine\Common\Collections\Collection
	 */
	protected $messages;

	/**
	 * @ORM\OneToMany(
	 *   targetEntity="MessageBundle\Entity\ThreadMetadata",
	 *   mappedBy="thread",
	 *   cascade={"all"}
	 * )
	 * @var ThreadMetadata[]|\Doctrine\Common\Collections\Collection
	 */
	protected $metadata;

	/**
	 * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Offer")
	 * @var Offer
	 */
	protected $offer;

	/**
	 * @return \DateTime
	 */
	public function getCreatedAt() {
		return $this->createdAt;
	}

	/**
	 * @param \DateTime $createdAt
	 */
	public function setCreatedAt(\DateTime $createdAt ) {
		$this->createdAt = $createdAt;
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId( $id ) {
		$this->id = $id;
	}

	/**
	 * @return ParticipantInterface
	 */
	public function getCreatedBy() {
		return $this->createdBy;
	}

	/**
	 * @param ParticipantInterface $createdBy
	 */
	public function setCreatedBy( ParticipantInterface $createdBy ) {
		$this->createdBy = $createdBy;
	}

	/**
	 * @return string
	 */
	public function getKeywords() {
		return $this->keywords;
	}

	/**
	 * @param string $keywords
	 */
	public function setKeywords( $keywords ) {
		$this->keywords = $keywords;
	}

	/**
	 * @return \Doctrine\Common\Collections\Collection|Message[]
	 */
	public function getMessages() {
		return $this->messages;
	}

	/**
	 * @param \Doctrine\Common\Collections\Collection|Message[] $messages
	 */
	public function setMessages( $messages ) {
		$this->messages = $messages;
	}

	/**
	 * @return \Doctrine\Common\Collections\Collection|ThreadMetadata[]
	 */
	public function getMetadata() {
		return $this->metadata;
	}

	/**
	 * @param \Doctrine\Common\Collections\Collection|ThreadMetadata[] $metadata
	 */
	public function setMetadata( $metadata ) {
		$this->metadata = $metadata;
	}

	/**
	 * @return Offer
	 */
	public function getOffer(){
		return $this->offer;
	}

	/**
	 * @param Offer $offer
	 */
	public function setOffer( $offer){
		$this->offer = $offer;
	}

}