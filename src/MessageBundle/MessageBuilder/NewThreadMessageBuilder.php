<?php
/**
 *
 * @package MessageBundle\MessageBuilder
 * @category    pha
 * @author      Bogdan SOOS <bogdan.soos@dynweb.org>
 * @copyright   2015 - 2015 pha
 * @version     SVN: $Id$
 *
 */

namespace MessageBundle\MessageBuilder;


use FOS\MessageBundle\Model\ParticipantInterface;
use UserBundle\Entity\User;
use UserBundle\Repository\UserRepository;

class NewThreadMessageBuilder extends \FOS\MessageBundle\MessageBuilder\AbstractMessageBuilder {

	/**
	 * @var Offer
	 */
	protected $offer;


	/**
	 * The thread subject
	 *
	 * @param  string
	 * @return NewThreadMessageBuilder (fluent interface)
	 */
	public function setOffer($offer)
	{
		$this->thread->setOffer($offer);

		return $this;
	}


	/**
	 * @param  array $recipients
	 * @return NewThreadMessageBuilder
	 */
	public function addRecipients( $recipients)
	{
		foreach ($recipients as $recipient) {
			$this->addRecipient($recipient);
		}

		return $this;
	}

	/**
	 * The thread subject
	 *
	 * @param  string
	 * @return NewThreadMessageBuilder (fluent interface)
	 */
	public function setSubject($subject)
	{
		$this->thread->setSubject($subject);

		return $this;
	}

	/**
	 * @param  ParticipantInterface $recipient
	 * @return NewThreadMessageBuilder (fluent interface)
	 */
	public function addRecipient( $recipient)
	{
		//$oUser = new UserRepository($recipient);
		//dump($oUser);die;

		$this->thread->addParticipant($recipient);

		return $this;
	}

}