<?php

namespace UserBundle\Helper;

use Symfony\Component\HttpFoundation\Session\Session;
use UserBundle\Entity\User;

class SecurityHelper
{
    /**
     * @param MailHelper $mailHelper
     */
    public function __construct(MailHelper $mailHelper, $twoStepAuthenticationEnabled)
    {
        $this->mailHelper = $mailHelper;
        $this->twoStepAuthenticationEnabled = $twoStepAuthenticationEnabled;
    }

    /**
     * @param Session $session
     * @param User    $user
     */
    public function generateAndSendCode(Session $session, $email)
    {
        $code = mt_rand(100000, 999999);

        //Set flag in the session
        $session->set('two_factor_authenticated', false);
        $session->set('two_factor_code', $code);

        $this->mailHelper->sendAuthenticationMail($email, $code);
        $this->sendCodeOnMobile($code);
    }

    /**
     * @param string $code
     */
    public function sendCodeOnMobile($code)
    {
        $message = 'Votre%20code%20de%20validation%20pour%20le%20portail%20achat%20EDF%20est%20le%20:%20'.$code;
        //AMA Free API
        $url  = 'https://smsapi.free-mobile.fr/sendmsg?user=90084347&pass=e1j1hksnwBCZoS&msg='.$message;
        $curl = curl_init($url);
        curl_exec($curl);
        curl_close($curl);
    }

    /**
     * @return bool
     */
    public function isTwoStepAuthenticationEnabled()
    {
        return $this->twoStepAuthenticationEnabled === true;
    }

    /**
     * @param string $codeInSession
     * @param string $code
     *
     * @return bool
     */
    public function codeIsValid($codeInSession, $code)
    {
        return $codeInSession == $code;
    }
}
