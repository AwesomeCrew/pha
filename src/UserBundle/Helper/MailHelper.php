<?php

namespace UserBundle\Helper;

use UserBundle\Entity\User;

class MailHelper
{
    public function __construct($mailer, $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function sendMail($subject, $to, $body)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom(array("robot@si0n.com" => "Si0n - Robot"))
            ->setTo($to)
            ->setContentType("text/html")
            ->setBody($body);

        $this->mailer->send($message);
    }

    /**
     * @param string $to
     * @param string $code
     */
    public function sendAuthenticationMail($to, $code)
    {
        $subject = '2 Step Authentication - Code';
        $body = $this->templating->render('UserBundle:Mail:twoStepAuthentication.html.twig', array(
            'code' => $code
        ));

        $this->sendMail($subject, $to, $body);
    }

    public function getName()
    {
        return 'MailHelper';
    }
}
