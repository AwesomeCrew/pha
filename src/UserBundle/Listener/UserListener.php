<?php

namespace UserBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use UserBundle\Entity\User;

class UserListener
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     *
     * @param LifecycleEventArgs $args
     */
    public function prePersist(User $entity, LifecycleEventArgs $args)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        if ($entity->getPassword() === null || $entity->getPassword() === 'NONE') {
            $entity->addRole($entity::ROLE_FRNS);
            $entity->setPlainPassword('mdp_'.$entity->getLastName());
            $entity->setEnabled(true);
            $userManager->updatePassword($entity);
        } else {
            $entity->addRole($entity::ROLE_EDF);
        }

    }


}