<?php

namespace UserBundle\Listener;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use UserBundle\Helper\SecurityHelper;

class RequestListener
{
    /**
     * Construct the listener
     * @param \Symfony\Component\Security\Core\SecurityContextInterface  $securityContext
     * @param \Symfony\Bundle\FrameworkBundle\Templating\EngineInterface $templating
     * @param \Symfony\Bundle\FrameworkBundle\Routing\Router $router
     */
    public function __construct(SecurityHelper $helper, TokenStorage $securityTokenStorage, EngineInterface $templating, Router $router)
    {
        $this->securityHelper = $helper;
        $this->securityTokenStorage = $securityTokenStorage;
        $this->templating = $templating;
        $this->router = $router;
    }

    /**
     * Listen for request events
     * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
     */
    public function onCoreRequest(GetResponseEvent $event)
    {
        $token   = $this->securityTokenStorage->getToken();
        $request = $event->getRequest();
        $session = $event->getRequest()->getSession();

        if (!$token)
        {
            return;
        }
        if (!$token instanceof UsernamePasswordToken)
        {
            return;
        }
        //If the two step authentication is activated
        if (!$this->securityHelper->isTwoStepAuthenticationEnabled()) {
            return;
        }
        //If the user is not authenticated (with first step)
        if (!$session->has('two_factor_authenticated'))
        {
            return;
        }
        //If the user is fully authenticated (with the two steps)
        if ($session->get('two_factor_authenticated') === true)
        {
            return;
        }

        if ($request->getMethod() == 'POST') {
            if ($request->get('resend_code', false)) {
                $user = $token->getUser();
                $this->securityHelper->generateAndSendCode($session, $user->getEmail());
                $session->getFlashBag()->set("success", "A new code has been sent.");
            } else {
                $codeInSession = $session->get('two_factor_code');

                //Check the authentication code
                if ($this->securityHelper->codeIsValid($codeInSession, $request->get('code')) == true) {
                    //Flag authentication complete
                    $session->set('two_factor_authenticated', true);

                    //Redirect to dashboard
                    $redirect = new RedirectResponse($this->router->generate("dashboard_index"));
                    $event->setResponse($redirect);

                    return;
                } else {
                    $session->getFlashBag()->set("error", "The verification code is not valid.");
                }
            }
        }

        //Force authentication code dialog
        $response = $this->templating->renderResponse('UserBundle:Security:twoFactorAuthentication.html.twig');
        $event->setResponse($response);
    }
}
