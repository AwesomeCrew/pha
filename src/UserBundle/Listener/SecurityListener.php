<?php

namespace UserBundle\Listener;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use UserBundle\Entity\User;
use UserBundle\Helper\SecurityHelper;

class SecurityListener
{
    /**
     * @param SecurityHelper $securityHelper
     */
    public function __construct(SecurityHelper $securityHelper)
    {
        $this->securityHelper = $securityHelper;
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        if (!$event->getAuthenticationToken() instanceof UsernamePasswordToken)
        {
            return;
        }
        //If the two step authentication is activated
        if (!$this->securityHelper->isTwoStepAuthenticationEnabled()) {
            return;
        }

        //Check if user can do two-factor authentication
        $token = $event->getAuthenticationToken();
        $user = $token->getUser();

        if (!$user instanceof User)
        {
            return;
        }

        $session = $event->getRequest()->getSession();

        //Send a new security code
        $this->securityHelper->generateAndSendCode($session, $user->getEmail());
    }
}
