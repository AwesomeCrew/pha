<?php
/**
 *
 * @package UserBundle\Repository
 * @category    pha
 * @author      Bogdan SOOS <bogdan.soos@dynweb.org>
 * @copyright   2015 - 2015 pha
 * @version     SVN: $Id$
 *
 */

namespace UserBundle\Repository;



use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\ChoiceList\LazyChoiceList;

class UserChoiceList extends LazyChoiceList {

	/**
	 * @var EntityManager
	 */
	protected $em;

	/**
	 * @var array
	 */
	private $aRoles = [];

	/**
	 * @param EntityManager $em
	 * @param $aRoles
	 */
	public function __construct(EntityManager $em, $aRoles){
		$this->em = $em;
		$this->aRoles = $aRoles;
	}

	/**
	 *
	 */
	protected function loadChoiceList()
	{
		$users = $this->em->getRepository('UserBundle:User')->findProviderUsers($this->aRoles);


		return new ChoiceList($users[0], $users[1]);
	}
}