<?php

/**
 *
 * @category    pha
 * @author      Bogdan SOOS <bogdan.soos@dynweb.org>
 * @copyright   2015 - 2015 pha
 * @version     SVN: $Id$
 *
 */

namespace UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use UserBundle\Entity\User;

/**
 * Class UserRepository.
 *
 * @package UserBundle\Repository
 */
class UserRepository extends EntityRepository {

	/**
	 * @return array
	 */
	public function findProviderUsers($aRoles)
	{

		$role = '';
		foreach ($aRoles as $sRole) {
			if ($sRole == User::ROLE_EDF) {
				$role = User::ROLE_FRNS;
				continue;
			}

			if ($sRole == User::ROLE_FRNS) {
				$role = User::ROLE_EDF;
				continue;
			}
		}

		$qb = $this->_em->createQueryBuilder();

		$aUsers = $qb->select('u')
		    ->from($this->_entityName, 'u')
		    ->where('u.roles LIKE :roles')
		    ->setParameter('roles', '%"'.$role.'"%')
			->getQuery()
			->getResult();


		$aIds = [];
		$aEmails = [];
		foreach ( $aUsers as $oUser ) {
			$aIds[$oUser->getUsername()] = $oUser;
			$aEmails[$oUser->getUsername()] = $oUser;
		}

		return [$aIds, $aEmails];
	}
}