<?php
/**
 * @author: Bogdan SOOS <bogdan.soos@gmail.com>
 */

namespace UserBundle\Entity;

use FOS\MessageBundle\Model\ParticipantInterface;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use MainBundle\Entity\Timestampable as Timestampable;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @ORM\HasLifecycleCallbacks
 * @ORM\EntityListeners({"UserBundle\Listener\UserListener"})
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 */
class User extends BaseUser implements ParticipantInterface {

    const ROLE_FRNS = 'ROLE_FRNS';
    const ROLE_EDF  = 'ROLE_EDF';

    use Timestampable;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 *
	 */
	public function __construct()
	{

		parent::__construct();
        $this->setCreatedAt(new \DateTime('now'));
        $this->setUpdatedAt(new \DateTime('now'));
		// your own logic
	}

	/**
     * @ORM\Column(type="string", name="first_name", nullable=true)
     * @var string
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", name="last_name", nullable=true)
     * @var string
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string", name="city", nullable=true)
     * @var string
     */
    protected $city;

    /**
     * @var datetime $birthDate
     *
     * @ORM\Column(type="datetime", name="birth_date", nullable=true)
     */
    protected $birthDate;

    /**
     * Image path
     *
     * @var string
     *
     * @ORM\Column(type="text", name="avatar", length=255, nullable=true)
     */
    protected $path;

	/**
	 * Image path
	 *
	 * @var string
	 *
	 * @ORM\Column(type="text", length=50, nullable=true)
	 */
	protected $phone;

    /**
    * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Provider", inversedBy="users")
    * @ORM\JoinColumn(name="provider_id", onDelete="CASCADE")
    */
    private $provider;

    /**
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\Response", cascade={"persist", "remove"}, mappedBy="user")
     * @Assert\Valid
     */
    private $responses;

    /**
     * @ORM\PrePersist
     */
    public function prePersist(){
        $this->setUsername($this->getEmail());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \MainBundle\Entity\Timestampable $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return \MainBundle\Entity\Timestampable
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

	/**
	 * @return string
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * @param string $phone
	 */
	public function setPhone( $phone )
	{
		$this->phone = $phone;
	}


    /**
     * Gets the value of provider.
     *
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Sets the value of provider.
     *
     * @param mixed $provider the provider
     *
     * @return self
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Gets the value of responses.
     *
     * @return mixed
     */
    public function getResponses()
    {
        return $this->responses;
    }
}