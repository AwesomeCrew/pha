<?php
/**
 * Created by PhpStorm.
 * User: bogdan
 * Date: 04/05/15
 * Time: 22:32
 */

namespace UserBundle\Controller;


use FOS\UserBundle\Controller\ResettingController as BaseResetting;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ResettingController. Controller managing the password forgot option.
 *
 * @package UserBundle\Controller
 */
class ResettingController extends BaseResetting {

	/**
	 * Request reset user password: show form
	 */
	public function requestAction(){
		return $this->render('UserBundle:Resetting:request.html.twig');
	}

	/**
	 * Request reset user password: submit form and send email
	 */
	public function sendEmailAction(Request $request)
	{
		$username = $request->request->get('username');

		/** @var $user UserInterface */
		$user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);

		if (null === $user) {
			return $this->render('UserBundle:Resetting:request.html.twig', array(
				'invalid_username' => $username
			));
		}

		if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
			$request->getSession()->getFlashBag()->add(
	            'warning',
	            $this->get('translator')->trans('resetting.password_already_requested', array(), 'UserBundle')
	        );

	        return new RedirectResponse($this->generateUrl('login'));

		}

		if (null === $user->getConfirmationToken()) {
			/** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
			$tokenGenerator = $this->get('fos_user.util.token_generator');
			$user->setConfirmationToken($tokenGenerator->generateToken());
		}

		$this->get('fos_user.mailer')->sendResettingEmailMessage($user);
		$user->setPasswordRequestedAt(new \DateTime());
		$this->get('fos_user.user_manager')->updateUser($user);

		$request->getSession()->getFlashBag()->add(
            'notice',
            $this->get('translator')->trans('resetting.check_email', array('%email%' => $this->getObfuscatedEmail($user)), 'UserBundle')
        );

        return new RedirectResponse($this->generateUrl('login'));

	}

	/**
	 * Tell the user to check his email provider
	 */
	public function checkEmailAction(Request $request)
	{
		$email = $request->query->get('email');

		if (empty($email)) {
			// the user does not come from the sendEmail action
			return new RedirectResponse($this->generateUrl('fos_user_resetting_request'));
		}

		return $this->render('UserBundle:Resetting:checkEmail.html.twig', array(
			'email' => $email,
		));
	}

	/**
	 * Reset user password
	 *
	 * @param Request $request
	 * @param $token
	 *
	 * @return RedirectResponse|null|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function resetAction(Request $request, $token){
		/** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
		$formFactory = $this->get('fos_user.resetting.form.factory');
		/** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
		$userManager = $this->get('fos_user.user_manager');
		/** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
		$dispatcher = $this->get('event_dispatcher');

		$user = $userManager->findUserByConfirmationToken($token);

		if (null === $user) {
			throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
		}

		$event = new GetResponseUserEvent($user, $request);
		$dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_INITIALIZE, $event);

		if (null !== $event->getResponse()) {
			return $event->getResponse();
		}

		$form = $formFactory->createForm();
		$form->setData($user);

		$form->handleRequest($request);

		if ($form->isValid()) {
			$event = new FormEvent($form, $request);
			$dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_SUCCESS, $event);

			$userManager->updateUser($user);

			if (null === $response = $event->getResponse()) {
		    	$request->getSession()->getFlashBag()->add(
		            'notice',
		            $this->get('translator')->trans('resetting.flash.success', array(), 'UserBundle')
		        );
				$url = $this->generateUrl('profile-edit');
				$response = new RedirectResponse($url);
			}

			$dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

			return $response;
		} else {
			$errors = $this->get('form_errors')->getFormErrors($form);
		}

		return $this->render('UserBundle:Resetting:reset.html.twig', array(
			'token' => $token,
			'form' => $form->createView(),
			'errors' => $errors
		));
	}

}