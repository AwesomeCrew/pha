<?php
/**
 * Created by PhpStorm.
 * User: bogdan
 * Date: 22/04/15
 * Time: 21:20
 */

namespace UserBundle\Controller;


use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class SecurityController. Override default security controller of FOS.
 *
 *
 * @package UserBundle\Controller
 */
class SecurityController extends BaseController {

	/**
	 * @param Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response|void
	 */
	public function loginAction( Request $request ) {

		if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
			$provider = $this->container->get('fos_message.provider');
			$this->container->get('session')->set('unreadNumberOfMessages', $provider->getNbUnreadMessages());

			return new RedirectResponse($this->generateUrl('dashboard_index'));
		}

		/** @var $session \Symfony\Component\HttpFoundation\Session\Session */
		$session = $request->getSession();

		// get the error if any (works with forward and redirect -- see below)
		if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
			$error = $request->attributes->get(SecurityContextInterface::AUTHENTICATION_ERROR);
		} elseif (null !== $session && $session->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
			$error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
			$session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
		} else {
			$error = null;
		}

		if (!$error instanceof AuthenticationException) {
			$error = null; // The value does not come from the security component.
		}

		// last username entered by the user
		$lastUsername = (null === $session) ? '' : $session->get(SecurityContextInterface::LAST_USERNAME);

		$csrfToken = $this->has('form.csrf_provider')
			? $this->get('form.csrf_provider')->generateCsrfToken('authenticate')
			: null;


		return $this->renderHomepage([
			'last_username' => $lastUsername,
			'error'         => $error,
			'csrf_token' => $csrfToken,
		]);
	}

	/**
	 * Renders the login template with the given parameters. Overwrite this function in
	 * an extended controller to provide additional data for the login template.
	 *
	 * @param array $data
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	protected function renderHomepage(array $data)
	{
		return $this->render('UserBundle:Security:login.html.twig', $data);
	}
}