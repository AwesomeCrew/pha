<?php

namespace UserBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProfileFormType extends AbstractType
{

    private $class;

    /**
     * @param string $class The User class name
     */
    public function __construct($class)
    {
        $this->class = $class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('current_password');
        $builder
            ->add('username', null, array(
                'label' => 'form.username',
                'translation_domain' => 'FOSUserBundle',
                'error_bubbling' => true))
            ->add('email', 'email', array(
                'label' => 'form.email',
                'translation_domain' => 'FOSUserBundle',
                'error_bubbling' => true))
            ->add('firstName', 'text', array(
                'label' => 'Nume',
                'error_bubbling' => true))
            ->add('lastName', 'text', array(
                'label' => 'Prenume',
                'error_bubbling' => true))
            ->add('city', 'text', array(
                'label' => 'Oras',
                'required' => false,
                'error_bubbling' => true))
            ->add('birthDate', 'date', array(
                'label' => 'Data de nastere',
                'attr' => array('class' => 'form-inline'),
                'widget' => 'single_text',
                'required' => false,
                'error_bubbling' => true));

        $builder->add('file', 'file', array('label' => 'Imagine', 'required' => false,));
    }

    /**
     * @return string
     */
    public function getParent() {
        return 'fos_user_profile';
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName() {
        return 'herbadoc_user_profile';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'intention'  => 'profile',
        ));
    }
}