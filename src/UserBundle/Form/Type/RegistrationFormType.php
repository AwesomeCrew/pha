<?php
/**
 * Created by PhpStorm.
 * User: bogdan
 * Date: 22/04/15
 * Time: 22:07
 */

namespace UserBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class RegistrationFormType.
 *
 * @package UserBundle\Form\Type
 */
class RegistrationFormType extends AbstractType {

	/**
	 * Override function to add custom field.
	 *
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options){
		$builder->remove('username');
		/*
		$builder->add('username', 'text', [
			'constraints' => array(
				new Assert\NotBlank([
					'message' => 'herba_user.username.blank'
				]),
				new Assert\Regex([
						'pattern' => '/^[A-Za-z0-9]+$/',
						'message' => 'Please use only letters and numbers.'
					]
				),
				new Assert\Length(['min' => 5, 'max' => 10])
			)
		]);*/
	}

	/**
	 * @return string
	 */
	public function getParent() {
		return 'fos_user_registration';
	}

	/**
	 * Returns the name of this type.
	 *
	 * @return string The name of this type
	 */
	public function getName() {
		return 'pha_user_registration';
	}
}